const express = require("express");
const bodyParser = require("body-parser");
const exSession = require("express-session");
const cookieParser = require("cookie-parser");
const cookieSession = require("cookie-session");
const keys = require("./config/keys");
const mongoose = require("mongoose");
const passport = require("passport");

const app = express();
app.set("view engine", "ejs");
require("./models/User");
require("./models/Ongaku");

require("./services/googlePassport");
require("./services/fbPassport");

//controllers
const login = require("./route/controllers/login");
const home = require("./route/controllers/home");
const logoutFromWeb = require("./route/controllers/logout");

//DATABASE CONNECTION
mongoose.connect(keys.mongoDb.dbConnectionString, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

//CONFIGURATION
app.set("view engine", "ejs");
app.use("/ext", express.static("ext"));
app.use("/bundle", express.static("bundle"));
app.use("/logic", express.static("logic"));

//MIDDLEWARES
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(exSession({ secret: 'my top secret', saveUninitialized: true, resave: false }));
app.use(cookieParser());

app.use(
  cookieSession({
    keydata: keys.cookie.gingersnap,
    keys: [keys.cookie.chocolate]
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.get("/", login);
app.get("/login", login);
app.get("/home", home);
app.get("/logout", logoutFromWeb);

require("./route/auth/authGoogle")(app);
require("./route/auth/authFacebook")(app);

//404 error route
app.use(function(req, res, next) {
  res.status(404).render("404", { title: "Sorry, page not found" });
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, (req, res) => {
  console.log("Server is running.......");
});

//浮遊魂
//Fuyū tamashī
