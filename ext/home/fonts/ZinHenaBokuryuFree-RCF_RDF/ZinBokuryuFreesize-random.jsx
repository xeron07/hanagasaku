var fontList = [
		"ZinHenaBokuryu-RCF",	// ジンへな墨流-RCF
		"ZinHenaBokuryu-RDF",	// ジンへな墨流-RDF
	];
var selObj = app.activeDocument.selection;
for(var i=0; i<selObj.length; i++){
	for(var j=0; j<selObj[i].characters.length; j++){
		var n = Math.floor(Math.random() * fontList.length);
		selObj[i].characters[j].textFont = app.textFonts[fontList[n]];
	}
}
var dx = 8;	// 左右のずれ
var dy = 8;	// 上下のずれ
var rot = 15;	// 最大20度
var size = 15;	// 最大15pt差
var selObj = app.activeDocument.selection;
for(var i=0; i<selObj.length; i++){
	for(var j=0; j<selObj[i].characters.length; j++){
		var x = Math.random() * dx - dx/2;
		var y = Math.random() * dy - dy/2;
		var r = Math.random() * rot - rot/2;
		var s = Math.random() * size - size/2;
		selObj[i].characters[j].baselineShift = y;
		selObj[i].characters[j].tracking = x;
		selObj[i].characters[j].rotation = r;
		var csize = selObj[i].characters[j].size;
		selObj[i].characters[j].size = csize + s;
	}
}