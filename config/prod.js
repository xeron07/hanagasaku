module.exports = {
  googleKeys: {
    apiKey: process.env.GOOGLE_CLIENT_ID,
    secretKey: process.env.GOOGLE_SECRET_ID
  },
  fbKey: {
    apiKey: process.env.FB_CLIENT_ID,
    secretKey: process.env.FB_SECRET_ID
  },
  mongoDb: {
    dbConnectionString: process.env.DB_URL
  },
  cookie: {
    chocolate: process.env.CHOCOLATE,
    gingersnap: process.env.GINGERSNAP,
    oatmeal: process.env.OATMEAL,
    macaroons: process.env.MACAROONS
  }
};
