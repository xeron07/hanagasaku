(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
console.clear();

class musicPlayer {
  constructor() {
    this.play = this.play.bind(this);
    this.playBtn = document.getElementById("play");
    this.playBtn.addEventListener("click", this.play);
    this.controlPanel = document.getElementById("control-panel");
    this.infoBar = document.getElementById("info");
  }

  play() {
    let controlPanelObj = this.controlPanel,
      infoBarObj = this.infoBar;
    Array.from(controlPanelObj.classList).find(function(element) {
      return element !== "active"
        ? controlPanelObj.classList.add("active")
        : controlPanelObj.classList.remove("active");
    });

    Array.from(infoBarObj.classList).find(function(element) {
      return element !== "active"
        ? infoBarObj.classList.add("active")
        : infoBarObj.classList.remove("active");
    });
  }
}

},{}],2:[function(require,module,exports){
require("./musicPlayer");

const newMusicplayer = new musicPlayer();

function Player(music) {
  this.isPlaying = true;
  this.music = music;
  this.next = null;
  this.prev = null;
  this.vol = 0.5;
  this.maxVol = 1.0;
  this.minVol = 0.0;
  this.willNextPlay = false;
  this.duration = 30000; // music duration in milisecond

  //methods
  (this.getMusic = () => {
    return this.music;
  }),
    (this.setMusic = obj => {
      this.music = obj;
    }),
    (this.play = () => {
      if (this.music === null) return null;
      this.music.play();
      newMusicplayer.play();
      this.isPlaying = true;
    }),
    (this.pause = () => {
      if (this.music === null) return null;
      this.music.pause();
      newMusicplayer.play();
      this.isPlaying = false;
    }),
    (this.nextMusic = () => {
      this.music.src = this.next;
      this.music.load();
      this.play();
    }),
    (this.prevMusic = () => {
      this.music.src = this.prev;
      this.music.load();
      this.play();
    }),
    (this.volUp = () => {
      if (this.vol < this.maxVol) vol += 0.1;
      else this.vol = 1.0;
      this.getVolume();
    }),
    (this.volDown = () => {
      if (this.vol > this.minVol) this.vol -= 0.1;
      else this.vol = 0.0;
      this.getVolume();
    }),
    (this.getVolume = () => {
      return this.vol;
    }),
    (this.toggleMusicPlayer = () => {
      if (isPlaying) this.pause();
      else this.play();
    }),
    (this.duractionControl = () => {});
}

},{"./musicPlayer":1}]},{},[1,2]);
