import React, { Component } from 'react';
class NavBar extends Component {
	state = {
		fontColor: '#c0392b'
	};
	render() {
		return (
			<div>
				<nav className="transparent fixed">
					<div className="nav-wrapper">
						<a href="#!" className="brand-logo">
							<img src="https://i.ibb.co/tYb2sJv/Webp-net-resizeimage.png" alt="Webp-net-resizeimage" />
						</a>
						<ul className="right hide-on-med-and-down">
							<li>
								<a href="sass.html" style={{ color: this.state.fontColor }}>
									search
								</a>
							</li>
							<li>
								<a href="badges.html" style={{ color: this.state.fontColor }}>
									view_module
								</a>
							</li>
							<li>
								<a href="/auth/google" style={{ color: this.state.fontColor }}>
									Registration
								</a>
							</li>
							<li>
								<a href="mobile.html" style={{ color: this.state.fontColor }}>
									more_vert
								</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		);
	}
}

export default NavBar;
