import React, { Component } from 'react';
import { dom } from '@fortawesome/fontawesome-svg-core';
import 'materialize-css/dist/css/materialize.css';
import 'materialize-css/dist/js/materialize';
import Navbar from './NavBar';
class App extends Component {
	render() {
		dom.watch();
		return (
			<div>
				<Navbar />
				<h1>Hello world</h1>
			</div>
		);
	}
}

export default App;
