const mongoose = require("mongoose");
const { Schema } = mongoose;

const ongakuSchema = new Schema({
  category: String,
  data: Array
});

mongoose.model("ongakus", ongakuSchema);
