function Player(music) {
  this.isPlaying = true;
  this.music = music;
  this.next = null;
  this.prev = null;
  this.vol = 0.5;
  this.maxVol = 1.0;
  this.minVol = 0.0;
  this.willNextPlay = false;
  this.duration = 30; // music duration in second
  this.musicLength = 0;
  // this.title = "Sakura";
  // this.artist = "unknown";
  this.timer = 0;
  //button uI control
  this.artist = "Eminem";
  this.title = "25 To Life";
  this.playBtn = document.getElementById("play");
  this.playBtn.addEventListener("click", this.toggleMusicPlayer);
  this.controlPanelObj = document.getElementById("control-panel");
  this.infoBarObj = document.getElementById("info");
  this.musicBar = document.getElementById("Bar");

  //methods
  this.getMusic = () => {
    return this.music;
  };
  this.setMusic = obj => {
    this.music = obj;
  };
  this.play = () => {
    if (this.music === null) return null;
    this.music.play();
    this.isPlaying = true;
    this.duractionControl();
    this.buttonToggler();
  };
  this.pause = () => {
    if (this.music === null) return null;
    this.music.pause();
    this.isPlaying = false;
    this.holdTimer();
    this.buttonToggler();
  };
  this.nextMusic = () => {
    this.music.src = this.next;
    this.music.load();
    this.play();
  };
  this.prevMusic = () => {
    this.music.src = this.prev;
    this.music.load();
    this.play();
  };
  this.volUp = () => {
    if (this.vol < this.maxVol) vol += 0.1;
    else this.vol = 1.0;
    this.getVolume();
  };
  this.volDown = () => {
    if (this.vol > this.minVol) this.vol -= 0.1;
    else this.vol = 0.0;
    this.getVolume();
  };
  this.getVolume = () => {
    return this.vol;
  };

  this.toggleMusicPlayer = () => {
    if (this.isPlaying) this.pause();
    else this.play();

    console.log("toggled");
  };

  this.playerControlPanel = element => {
    if (this.isPlaying) {
      if (element !== "active")
        return this.controlPanelObj.classList.add("active");
    } else {
      if (element === "active")
        return this.controlPanelObj.classList.remove("active");
    }
  };

  this.playerInfoPanel = element => {
    if (this.isPlaying) {
      if (element !== "active") return this.infoBarObj.classList.add("active");
    } else {
      if (element === "active")
        return this.infoBarObj.classList.remove("active");
    }
  };

  this.buttonToggler = async () => {
    await Array.from(this.controlPanelObj.classList).find(element => {
      return this.playerControlPanel(element);
    });

    await Array.from(this.infoBarObj.classList).find(element => {
      return this.playerInfoPanel(element);
    });
    //console.clear();
  };
  this.changeMusicBar = () => {
    let widthValue = (100 / this.duration) * (this.musicLength / 1000) + "%";
    this.musicBar.style.width = widthValue;
    console.log(widthValue + " " + this.musicLength);
  };
  this.duractionControl = () => {
    if (this.musicLength < this.duration * 1000) {
      this.musicLength += 1000;
      this.changeMusicBar();
    } else {
      this.holdTimer();
      this.musicLength = 0;
      this.changeMusicBar();
    }

    this.timer = setTimeout(() => {
      this.duractionControl();
    }, 1000);
  };
  this.holdTimer = () => {
    clearTimeout(this.timer);
    this.timer = 0;
  };

  this.playMusic = (obj, artist, title) => {
    //will be implement
    this.music.src = obj;
    this.duration = 31;
    this.holdTimer();
    this.musicLength = 0;
    this.music.load();
    this.play();
    this.artist = artist;
    this.title = title;
    this.changeTitle();
    console.log("music src:" + this.music.src);

    console.log(obj);
  };
  this.changeTitle = () => {
    document.getElementById("artistName").innerText = this.artist;
    document.getElementById("titleVal").innerText = this.title;
  };
}
