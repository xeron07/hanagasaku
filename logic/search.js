artistSearch = async (name) => {
	let searchString = `https://api.deezer.com/search/artist/?q=${name}&index=0&limit=2&output=json`;
	$.ajax({
		url: searchString,
		method: 'GET',
		success: (result) => {
			console.log(result);
		},
		error: (err) => {
			console.error(err);
		}
	});
};
