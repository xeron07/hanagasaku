const passport = require("passport");
const FacebookStrategy = require("passport-facebook").Strategy;
const keys = require("../config/keys");
const mongoose = require("mongoose");
const User = mongoose.model("users");
//const userCollection = require('../models/User');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id).then(user => {
    done(null, user);
  });
});

passport.use(
  new FacebookStrategy(
    {
      clientID: keys.fbKey.apiKey,
      clientSecret: keys.fbKey.secretKey,
      callbackURL: "/auth/google/callback",
      proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
      console.log(profile);

      const userExist = await User.findOne({ userId: profile.id });
      if (userExist) done(null, userExist);
      else {
        const newUser = await new User({
          userId: profile.id,
          name: profile.displayName,
          email: "no-email",
          profileImg:
            "https://i.ibb.co/C7z4FsD/500-F-118033377-JKQA3-UFE4jo-J1k67d-No-Smmo-G4-Es-Qf9-Ho.jpg",
          signUpDate: new Date().toLocaleDateString()
        }).save();

        done(null, newUser);
        alert("user added");
        console.log(user);
      }
    }
  )
);
