const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const keys = require("../config/keys");
const mongoose = require("mongoose");
const User = mongoose.model("users");
//const userCollection = require('../models/User');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id).then(user => {
    done(null, user);
  });
});

passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleKeys.apiKey,
      clientSecret: keys.googleKeys.secretKey,
      callbackURL: "/auth/google/callback",
      proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
      console.log(profile);

      const userExist = await User.findOne({ userId: profile.id });
      if (userExist) done(null, userExist);
      else {
        const newUser = await new User({
          userId: profile.id,
          name: profile.displayName,
          email: profile.emails[0].value,
          profileImg: profile.photos[0].value,
          signUpDate: new Date().toLocaleDateString()
        }).save();

        done(null, newUser);
        alert("user added");
        console.log(user);
      }
    }
  )
);
