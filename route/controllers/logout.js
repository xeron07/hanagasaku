const express = require("express");
const router = express.Router();

router.get("*", (req, res, next) => {
  if (!req.user) {
    res.redirect("/login");
  } else {
    next();
  }
});

router.get("/logout", (req, res) => {
  req.logout();
  res.redirect("/login");
});
module.exports = router;
