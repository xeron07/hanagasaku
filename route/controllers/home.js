const express = require("express");
require("../../logic/execution");
const router = express.Router();

router.get("*", (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.redirect("/login");
  }
});

router.get("/home", async (req, res) => {
  let userData = req.user;
  const musics = await fetchAllMusics();
  console.log(musics[3].data[0].title);
  const dataObj = {
    userData: userData,
    musicsData: musics
  };
  await res.render("home/index", dataObj);
});

module.exports = router;
