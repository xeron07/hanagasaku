const express = require('express');
const router = express.Router();

router.get('*', (req, res, next) => {
	if (req.user) {
		res.redirect('/home');
	} else {
		next();
	}
});

router.get('/login', (req, res) => {
	res.render('login/index');
});

module.exports = router;
